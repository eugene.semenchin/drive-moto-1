$(function(){

  $('.banner-section-slider').slick({
    dots: true,
    prevArrow: '<button class="banner-section-slider-btn banner-section-slider-btn-prev"><img src="images/svg/arrow-left.svg" alt="arrow-left"></button>',
    nextArrow: '<button class="banner-section-slider-btn banner-section-slider-btn-next"><img src="images/svg/arrow-right.svg" alt="arrow-right"></button>',

    responsive: [
      {
        breakpoint: 969,
        settings: {
          arrows: false
        }
      }
    ]
  });

  $('.tab').on('click', function(e){
    e.preventDefault();

    $($(this).siblings()).removeClass('tab--active');
    $($(this).closest('.tabs-wrapper').siblings().find('div')).removeClass('tabs-content--active');

    $(this).addClass('tab--active');
    $($(this).attr('href')).addClass('tabs-content--active');

    $('.product-slider').slick('setPosition');
  });

  $('.product-item-favorite').on('click', function(){
    $(this).toggleClass('product-item-favorite--active');
  });

  $('.product-slider').slick({
    slidesToShow: 4,
    slidesToScroll: 1,
    prevArrow: '<button class="product-slider-slider-btn product-slider-slider-btn-prev"><img src="images/svg/arrow-black-left.svg" alt="arrow-black-left"></button>',
    nextArrow: '<button class="product-slider-slider-btn product-slider-slider-btn-next"><img src="images/svg/arrow-black-right.svg" alt="arrow-black-right"></button>',
    responsive: [
      {
        breakpoint: 1301,
        settings: {
          arrows: false,
          dots: true
        }
      },
      {
        breakpoint: 1201,
        settings: {
          arrows: false,
          slidesToShow: 3,
          dots: true
        }
      },
      {
        breakpoint: 870,
        settings: {
          arrows: false,
          slidesToShow: 2,
          dots: true
        }
      },
      {
        breakpoint: 590,
        settings: {
          arrows: false,
          slidesToShow: 1,
          dots: true
        }
      },
    ]
  })

  $('.filter-style').styler();
  $('.filter-item-drop, filter-extra').on('click', function(){
    $(this).toggleClass('filter-item-drop--active');
    $(this).next().slideToggle('200');
  });

  $('.js-range-slider').ionRangeSlider({
    type: 'double',
    min: 100000,
    max: 500000,
  });

  $('.catalog-filter-btn-grid').on('click', function(){
    $(this).addClass('.catalog-filter-button--active');
    $('.catalog-filter-btn-line').removeClass('.catalog-filter-button--active');
    $('product-item-wrapper').removeClass('product-item-wrapper--list');

  });

  $('.catalog-filter-btn-line').on('click', function(){
    $(this).addClass('.catalog-filter-button--active');
    $('.catalog-filter-btn-grid').removeClass('.catalog-filter-button--active');
    $('product-item-wrapper').addClass('product-item-wrapper--list');
  });

  $('.rate-yo').rateYo({
    ratedFill: '#1C62CD',
    normalFill: '#C4C4C4',
    spacing: '7px'
  });

  $('.menu-btn').on('click', function(){
    $('.menu-mobile-list').toggleClass('menu-mobile-list--active')
  });

  $('.footer-top-drop').on('click', function(){
    $(this).next().slideToggle();
    $(this).toggleClass('footer-top-drop--active');
  });

  $('.aside-btn').on('click', function(){
    $(this).next().slideToggle();
  });

});


